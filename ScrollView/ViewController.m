//
//  ViewController.m
//  ScrollView
//
//  Created by Zachary Cole on 1/13/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIScrollView* sv = [UIScrollView new];
    sv.frame = self.view.bounds;
    sv.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:sv];
    sv.contentSize = CGSizeMake(sv.frame.size.width * 3, sv.frame.size.height * 3);
    
    // Row A, Cols 1, 2, 3
    UIView* a1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, sv.frame.size.width,
                                                         sv.frame.size.height)];
    a1.backgroundColor = [UIColor blueColor];
    [sv addSubview:a1];

    UIView* a2 = [[UIView alloc]initWithFrame:CGRectMake(sv.frame.size.width, 0, sv.frame.size.width, sv.frame.size.height)];
    a2.backgroundColor = [UIColor redColor];
    [sv addSubview:a2];
    
    UIView* a3 = [[UIView alloc]initWithFrame:CGRectMake(sv.frame.size.width * 2, 0, sv.frame.size.width, sv.frame.size.height)];
    a3.backgroundColor = [UIColor yellowColor];
    [sv addSubview:a3];
    
    // Row B, Cols 1, 2, 3
    UIView* b1 = [[UIView alloc]initWithFrame:CGRectMake(0, sv.frame.size.height, sv.frame.size.width, sv.frame.size.height)];
    b1.backgroundColor = [UIColor redColor];
    [sv addSubview:b1];
    
    UIView* b2 = [[UIView alloc]initWithFrame:CGRectMake(sv.frame.size.width, sv.frame.size.height, sv.frame.size.width, sv.frame.size.height)];
    b2.backgroundColor = [UIColor yellowColor];
    [sv addSubview:b2];
    
    UIView* b3 = [[UIView alloc]initWithFrame:CGRectMake(sv.frame.size.width * 2, sv.frame.size.height, sv.frame.size.width, sv.frame.size.height)];
    b3.backgroundColor = [UIColor blueColor];
    [sv addSubview:b3];
    
    // Row C, Cols 1, 2, 3
    UIView* c1 = [[UIView alloc]initWithFrame:CGRectMake(0, sv.frame.size.height * 2, sv.frame.size.width, sv.frame.size.height)];
    c1.backgroundColor = [UIColor yellowColor];
    [sv addSubview:c1];
    
    UIView* c2 = [[UIView alloc]initWithFrame:CGRectMake(sv.frame.size.width, sv.frame.size.height * 2, sv.frame.size.width, sv.frame.size.height)];
    c2.backgroundColor = [UIColor blueColor];
    [sv addSubview:c2];
    
    UIView* c3 = [[UIView alloc]initWithFrame:CGRectMake(sv.frame.size.width * 2, sv.frame.size.height * 2, sv.frame.size.width, sv.frame.size.height)];
    c3.backgroundColor = [UIColor redColor];
    [sv addSubview:c3];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
